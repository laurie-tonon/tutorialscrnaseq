
<!-- README.md is generated from README.Rmd. Please edit that file -->

# tutorialSCRNAseq

<!-- badges: start -->
<!-- badges: end -->

The goal of tutorialSCRNAseq is to learn the first steps of single cell
RNAseq analysis with Seurat, without programming skills.

## Installation

You can install the development version of tutorialSCRNAseq from
[GitLab](https://gitlab.com/) with:

``` r
# install.packages("devtools")
devtools::install_gitlab("laurie-tonon/tutorialSCRNAseq")
```
